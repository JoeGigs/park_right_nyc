class ChangeTimeDataTypeToTime < ActiveRecord::Migration[5.0]
  def change
  	change_column :streets, :start_time, :time
  	change_column :streets, :end_time, :time
  end
end
