class AddBoroughToStreet < ActiveRecord::Migration[5.0]
  def change
  	add_column :streets, :borough, :string
  end
end
