class ChangeTimeDataTypeToDatEtime < ActiveRecord::Migration[5.0]
  def change
  	change_column :streets, :start_time, :datetime 
  	change_column :streets, :end_time, :datetime
  end
end
