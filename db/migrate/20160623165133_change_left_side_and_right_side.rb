class ChangeLeftSideAndRightSide < ActiveRecord::Migration[5.0]
  def change
  	change_column :streets, :left_side, :string
  	change_column :streets, :right_side, :string
  end
end
