class AddStartTimeAndEndTimeToStreets < ActiveRecord::Migration[5.0]
  def change
  	add_column :streets, :start_time, :string
  	add_column :streets, :end_time, :string
  end
end
