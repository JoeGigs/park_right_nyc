class ChangeLeftRightSide < ActiveRecord::Migration[5.0]
  def change
  	change_column :streets, :start_time, :integer
  	change_column :streets, :end_time, :integer
  end
end
