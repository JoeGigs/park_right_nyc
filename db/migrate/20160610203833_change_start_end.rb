class ChangeStartEnd < ActiveRecord::Migration[5.0]
  def change
  	change_column :streets, :left_side, :integer
  	change_column :streets, :right_side, :integer
  end
end
