class StreetsController < ApplicationController

  def console
    @streets = Street.all
  end

  def new
    @street = Street.new
  end

  def create
    @streets = Street.all
    @street = Street.new(street_params)
    respond_to do |format|
      if @street.save
        format.js
      end
    end
  end
  
  def show
    @street = Street.find(params[:id])
  end

  def search
    @streets = Street.all
    respond_to do |format|
      if params[:search]
    @streets = Street.search(params[:search]).order("created_at DESC")
    format.js
      else
    @streets = Street.all.order("created_at DESC")
    format.js
    end
  end
end

  def edit
    
  end

  def update

  end

  def destroy
  end

  private

  def street_params
    params.require(:street).permit(:name, :borough, :left_side, :right_side, :start_time, :end_time)
  end
end
