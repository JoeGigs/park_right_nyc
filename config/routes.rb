Rails.application.routes.draw do
  root 'streets#console'
  resources :streets, except: [:index, :create, :new]
  
  get '/streets', to: 'streets#console', as: :streets_index
  get '/new_street', to: 'streets#new'
  post '/new_street', to: 'streets#create'

  get '/search', to: 'streets#search', as: :streets_search
end
